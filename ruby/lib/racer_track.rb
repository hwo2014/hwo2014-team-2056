module Racer
  class Track
    def initialize
      @pieces = []
    end

    def add_pieces(data)
      @pieces << data['race']['track']['pieces']
    end

    def switch_lane(data, tcp)
      current_piece = data[0]['piecePosition']['pieceIndex']
      next_piece =  next_piece(current_piece)

      unless next_piece.nil?
        if next_piece.has_key?("radius")
          if next_piece['angle'] > 0
            puts ">>     Switch Lane: Right"
            tcp.puts lane_message("Right")
          else
            puts ">>     Switch Lane: Left"
            tcp.puts lane_message("Left")
          end
        end
      end
    end

    def current_piece(position_piece)
      @pieces[0][position_piece]
    end

    def next_piece(position_piece)
      @pieces[0][position_piece+1]
    end

    private
    def lane_message(lane)
      JSON.generate({:msgType => "switchLane", :data => lane})
    end

    def piece_switch(piece, current_position_piece)
      if piece.has_key?("switch")
        find_next_piece_angle(current_position_piece)
      else
        false
      end
    end

    def find_next_piece_angle(current_position_piece)
      piece = next_piece(current_position_piece+=1)

      while(!piece.nil?) do
        break if piece.has_key?("angle")
        piece = next_piece(current_position_piece+=1)
      end

      if piece.nil?
        false
      else
        piece['angle']
      end
    end
  end
end