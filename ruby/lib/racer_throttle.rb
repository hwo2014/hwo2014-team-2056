require_relative 'racer_track'

module Racer
  class Throttle
    def initialize
      @acceleration = 1.0
      @angle = 0.0
    end

    def set_acceleration(data, track)
      @angle            = data[0]['angle']
      position_piece    = data[0]['piecePosition']['pieceIndex']
      in_piece_distance = data[0]["piecePosition"]["inPieceDistance"]
      current_piece     = track.current_piece(position_piece)
      next_piece        = track.next_piece(position_piece)
      next_piece_2      = track.next_piece(position_piece+1)

      puts ">>       Car Angle: #{@angle}"
      puts ">>         Current: #{current_piece.has_key?('length') ? 'Reta' : 'Curva'}"
      puts ">>            Next: #{next_piece.has_key?('length') ? 'Reta' : 'Curva' unless next_piece.nil?}"

      throttle_through_angle_of_car

      unless next_piece.nil?
        if current_piece.has_key?("length") && (!next_piece_2.nil? && next_piece_2.has_key?("radius"))
          if (in_piece_distance >= (current_piece["length"]*0.1)) && greater_angle(next_piece_2["angle"], 30)
            @acceleration = 0.0
          else
            @acceleration = 0.45
          end

        elsif next_piece.has_key?("radius") && current_piece.has_key?("radius")
          if greater_angle(next_piece["angle"], 30)
            @acceleration = 0.45
          else
            @acceleration = 1.0
          end

        elsif next_piece.has_key?("length")
          @acceleration = 1.0
        end
      end
    end

    def throttle_through_angle_of_car
      if @angle == 0.0
        @acceleration = 1.0
      elsif @angle.between?(-10, 40)
        add_throttle(0.05)
      else
        @acceleration = 0.0
      end
    end

    def greater_angle(angle, comparator)
      angle > comparator || angle < -comparator
    end

    def subtract_throttle(value)
      @acceleration -= value
      fallback_throttle
    end

    def add_throttle(value)
      @acceleration += value
      fallback_throttle
    end

    def fallback_throttle
      if @acceleration > 1.0
        @acceleration = 1.0
      elsif @acceleration < 0.0
        @acceleration = 0.0
      end
    end

    def get_acceleration
      puts ">>    Acceleration: #{@acceleration}"
      @acceleration
    end

    def get_angle
      @angle
    end
  end
end