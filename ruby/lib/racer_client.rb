require 'socket'
require 'rubygems'
require 'json'
require 'fileutils'

require_relative 'racer_throttle'
require_relative 'racer_track'

module Racer
  class Client
    def initialize(server_host, server_port, bot_name, bot_key)
      tcp = TCPSocket.open(server_host, server_port)
      play(bot_name, bot_key, tcp)
    end

    private

    def play(bot_name, bot_key, tcp)
      tcp.puts join_message(bot_name, bot_key)
      react_to_messages_from_server tcp
    end

    def react_to_messages_from_server(tcp)
      throttle = Racer::Throttle.new
      track = Racer::Track.new

      while json = tcp.gets
        message = JSON.parse(json)
        msgType = message['msgType']
        msgData = message['data']
        msgGameTick = message['gameTick']

        case msgType
          when 'carPositions'
            # puts "carPositions"
            puts ">>     msgGameTick: #{msgGameTick}"
            puts ">>      pieceIndex: #{msgData[0]['piecePosition']['pieceIndex']}"
            puts ">> inPieceDistance: #{msgData[0]['piecePosition']['inPieceDistance']} \n\n"
            track.switch_lane(msgData, tcp)
            throttle.set_acceleration(msgData, track)
            tcp.puts throttle_message(throttle.get_acceleration)

          when 'join'
            puts 'Joined'
          when 'yourCar'
            puts 'yourCar'
          when 'gameInit'
            puts 'gameInit'
            track.add_pieces(msgData)
          when 'gameStart'
            puts 'Race started'
          when 'crash'
            puts 'Someone crashed'
            puts "acceleration: #{throttle.get_acceleration}"
            puts "angle: #{throttle.get_angle}"
          when 'spawn'
            puts 'spawn'
            tcp.puts throttle_message(0.6)
          when 'lapFinished'
            puts 'Lap finished'
          when 'dnf'
            puts 'Car is disqualified'
          when 'finish'
            puts 'car finishes the race'
          when 'gameEnd'
            puts 'Race ended'
          when 'tournamentEnd'
            puts 'Tournament ended'
          when 'error'
            puts "ERROR: #{msgData}"
          else
            puts "Unknown msgType: #{msgType}"
        end

        # puts "#{msgType}: #{message} \n\n"
        tcp.puts ping_message
      end
    end

    def join_message(bot_name, bot_key)
      make_msg("join", {:name => bot_name, :key => bot_key})
    end

    def throttle_message(throttle)
      make_msg("throttle", throttle)
    end

    def ping_message
      make_msg("ping", {})
    end

    def make_msg(msgType, data)
      JSON.generate({:msgType => msgType, :data => data})
    end
  end
end